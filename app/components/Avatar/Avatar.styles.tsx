import styled from 'styled-components'

const AvatarBody = styled.div`
  display: inline-flex;
  border-radius: 100%;
  user-select: none;
  font-size: 1rem;
  position: relative;
  transition: opacity 0.5s ease;

  .sb-avatar {
    transition: opacity 0.5s ease;
    cursor: pointer;

    &:hover {
      opacity: 0.75;
    }
  }
`

const AvatarDropdown = styled.div`
  padding: 0.5rem;
  display: flex;
  flex-direction: column;
  border-radius: 0.5rem;
  position: absolute;
  top: 100%;
  right: 0;
  transform: translateY(0.5rem);
  background-color: var(--white);
  box-shadow: var(--box_shadow);
`

const DropdownLink = styled.div`
  padding: 0.5rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 0.5rem;
  border-radius: 0.5rem;
  color: var(--danger);
  cursor: pointer;
  transition: background-color 0.5s ease;

  &:hover {
    background-color: var(--grey_200);
  }
`

export { AvatarBody, AvatarDropdown, DropdownLink }
