import Icon from '@app/components/Icon/Icon'
import { faRightFromBracket } from '@fortawesome/free-solid-svg-icons'
import { userService } from '@services'
import 'core-js-pure/stable/object/assign'
import 'core-js/es7/array'
import { Variants, motion } from 'framer-motion'
import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react'
import Avatar from 'react-avatar'
import 'regenerator-runtime/runtime'
import { AvatarBody, AvatarDropdown, DropdownLink } from './Avatar.styles'
import { IAvatar } from './Avatar.types'

const dropdownVariants: Variants = {
  open: {
    display: 'flex',
    visibility: 'visible',
    transition: {
      staggerChildren: 0.17,
      delayChildren: 0.2,
    },
  },
  closed: {
    display: 'none',
    transition: {
      staggerChildren: 0.05,
      staggerDirection: -1,
      when: 'afterChildren',
    },
  },
}

function AvatarComp({ size, onClick }: IAvatar) {
  const [firstName, setFirstName] = useState<string>('')
  const [lastName, setLastName] = useState<string>('')
  const [dropdownOpen, setDropdownOpen] = useState<boolean>(false)
  const dropdownRef = useRef<any>()

  const Router = useRouter()

  useEffect(() => {
    const handleOutsideClick = (event: any) => {
      if (!dropdownRef.current) {
        return
      }
      if (!dropdownRef.current.contains(event.target)) {
        setDropdownOpen(false)
      }
    }

    window.addEventListener('mousedown', handleOutsideClick)

    return () => {
      window.removeEventListener('mousedown', handleOutsideClick)
    }
  }, [dropdownRef])

  useEffect(() => {
    const userData = userService.userValue
    setFirstName(userData.firstName)
    setLastName(userData.lastName)
  }, [])

  const handleLogout = () => {
    userService.logout()

    Router.push('/')
  }

  return (
    <AvatarBody ref={dropdownRef}>
      <Avatar
        name={`${firstName} ${lastName}`}
        maxInitials={2}
        size={size || '48'}
        round
        title={false}
        onClick={() => (onClick ? onClick : setDropdownOpen(true))}
      />

      <motion.div
        variants={dropdownVariants}
        animate={dropdownOpen ? 'open' : 'closed'}>
        <AvatarDropdown>
          <DropdownLink onClick={handleLogout}>
            <Icon icon={faRightFromBracket} />
            <span>Ausloggen</span>
          </DropdownLink>
        </AvatarDropdown>
      </motion.div>
    </AvatarBody>
  )
}

export default AvatarComp
