export interface IAvatar {
  size?: string
  onClick?: () => void
}
