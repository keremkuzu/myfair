import styled from 'styled-components'
import { IButtonStyles } from './Button.types'

const Button = styled.button<IButtonStyles>`
  padding: ${props =>
    props.buttonType === 'icon'
      ? '8px'
      : props.nopaddinghorizontal && 'xs'
      ? '4px 0'
      : props.nopaddinghorizontal && 's'
      ? '8px 0'
      : props.nopaddinghorizontal && 'm'
      ? '12px 0'
      : props.nopaddinghorizontal && 'l'
      ? '16px 0'
      : props.padding === 'xs'
      ? '4px 8px'
      : props.padding === 's'
      ? '8px 12px'
      : props.padding === 'm'
      ? '12px 16px'
      : props.padding === 'l'
      ? '16px 24px'
      : '8px 12px'}; // Fallback to S as default
  font-size: ${props =>
    props.fontSize === 'xs'
      ? '0.75rem'
      : props.fontSize === 's'
      ? '1rem'
      : props.fontSize === 'm'
      ? '1.25rem'
      : props.fontSize === 'l'
      ? '1.5rem'
      : '1rem'}; // Fallback to S as default
  color: ${props => `${props.fontColor}`};
  background-color: ${props => `${props.backgroundColor}`};
  outline: none;
  border: ${props => `1px solid ${props.borderColor}`};
  border-radius: ${props =>
    props.borderRadius ? props.borderRadius : '.375rem'};
  display: flex;
  cursor: pointer;
  transition: all 250ms ease;

  &:hover {
    opacity: 0.8;
    text-decoration: ${props => (props.link ? 'underline' : 'none')};
    background-color: ${props =>
      props.backgroundHover === 'success'
        ? 'rgba(46, 204, 113, .125)'
        : props.backgroundHover === 'warning'
        ? 'rgba(230, 126, 34, .125)'
        : props.backgroundHover === 'danger'
        ? 'rgba(231, 76, 60, .125)'
        : props.backgroundColor};
  }

  &:active {
    opacity: 0.5;
  }
`

export { Button }
