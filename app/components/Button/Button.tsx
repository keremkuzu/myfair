import { useState, useEffect } from 'react'
import { Button as Btn } from './Button.styles'
import type { IButton } from './Button.types'

const Button = ({
  children,
  btnSize,
  btnType,
  btnLevel,
  onClick,
  borderRadius,
  type,
  npH,
  ...props
}: IButton): JSX.Element => {
  const [btnBG, setBtnBG] = useState('transparent')
  const [btnColor, setBtnColor] = useState('white')
  const [btnBorder, setBtnBorder] = useState('transparent')

  const handleBGColor = (color: string) => {
    setBtnBG(color)
  }

  const handleBorderColor = (color: string) => {
    setBtnBorder(color)
  }

  const handleFontColor = (color: string) => {
    setBtnColor(color)
  }

  useEffect(() => {
    switch (btnType) {
      case 'solid':
        handleFontColor('white')
        if (btnLevel === 'success') handleBGColor('#2ECC71')
        else if (btnLevel === 'warning') handleBGColor('#E67E22')
        else if (btnLevel === 'danger') handleBGColor('#E74C3C')
        else handleBGColor('#7098BF')
        break
      case 'contrast':
        handleBGColor('#242424')
        handleBorderColor('transparent')
        handleFontColor('white')
        break
      case 'outline':
        handleBGColor('transparent')
        handleFontColor('#7098BF')
        if (btnLevel === 'success') {
          handleBorderColor('#2ECC71')
          handleFontColor('#2ECC71')
        } else if (btnLevel === 'warning') {
          handleBorderColor('#E67E22')
          handleFontColor('#E67E22')
        } else if (btnLevel === 'danger') {
          handleBorderColor('#E74C3C')
          handleFontColor('#E74C3C')
        } else handleBorderColor('#7098BF')
        break
      case 'ghost':
        handleBGColor('transparent')
        handleBorderColor('transparent')
        if (btnLevel === 'success') handleFontColor('#2ECC71')
        else if (btnLevel === 'warning') handleFontColor('#E67E22')
        else if (btnLevel === 'danger') handleFontColor('#E74C3C')
        else handleFontColor('#7098BF')
        break
      case 'icon':
        handleBGColor('transparent')
        handleBorderColor('transparent')
        if (btnLevel === 'success') handleFontColor('#2ECC71')
        else if (btnLevel === 'warning') handleFontColor('#E67E22')
        else if (btnLevel === 'danger') handleFontColor('#E74C3C')
        else handleFontColor('#7098BF')
        break
      case 'link':
        handleBGColor('transparent')
        handleBorderColor('transparent')
        if (btnLevel === 'success') handleFontColor('#2ECC71')
        else if (btnLevel === 'warning') handleFontColor('#E67E22')
        else if (btnLevel === 'danger') handleFontColor('#E74C3C')
        else handleFontColor('#7098BF')
        break
    }
  }, [btnLevel, btnType])

  return (
    <Btn
      {...props}
      type={type}
      buttonType={btnType}
      fontSize={btnSize || 's'}
      padding={btnSize || 'icon' || 's'}
      fontColor={btnColor}
      backgroundColor={btnBG}
      borderRadius={borderRadius}
      backgroundHover={
        btnType === 'ghost' || btnType === 'icon' ? btnLevel : undefined
      }
      borderColor={btnBorder}
      link={btnType === 'link'}
      nopaddinghorizontal={npH}
      onClick={onClick}>
      {children}
    </Btn>
  )
}

export default Button
