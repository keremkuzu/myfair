export type IButtonStyles = {
  backgroundColor: string
  backgroundHover: 'success' | 'warning' | 'danger' | undefined
  borderColor: string
  link: boolean
  fontSize: 'xs' | 's' | 'm' | 'l'
  padding: 'xs' | 's' | 'm' | 'l' | 'icon'
  buttonType: 'solid' | 'contrast' | 'outline' | 'ghost' | 'link' | 'icon'
  borderRadius: string | undefined
  fontColor: string
  iconLeft?: boolean
  iconRight?: boolean
  nopaddinghorizontal?: boolean
}

export interface IButton {
  type?: 'button' | 'submit' | 'reset' | undefined
  children: React.ReactNode
  borderRadius?: string
  btnSize: 'xs' | 's' | 'm' | 'l'
  btnType: 'solid' | 'contrast' | 'outline' | 'ghost' | 'link' | 'icon'
  btnLevel?: 'success' | 'warning' | 'danger'
  npH?: boolean
  onClick?: () => void
}
