import styled from 'styled-components'
import {
  ISelectChevronStyles,
  ISelectDropdownStyles,
  ISelectStyles,
} from './Select.types'

const SelectBody = styled.div<ISelectStyles>`
  width: ${({ fullWidth, width }) =>
    fullWidth ? '100%' : width ? width : 'auto'};
  display: flex;
  flex-direction: row;
  align-items: center;
`

const SelectLabel = styled.p`
  padding-right: 8px;
`

const SelectView = styled.div`
  position: relative;
`

const SelectInput = styled.div<ISelectStyles>`
  width: ${({ width }) => (width ? `${width}px` : 'auto')};
  padding: 8px 12px;
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 1rem;
  font-size: 1rem;
  color: ${({ danger }) => (danger ? 'var(--danger)' : 'var(--grey_900)')};
  border: none;
  border-bottom: ${({ active, danger }) =>
    active
      ? '1px solid var(--cta)'
      : danger
      ? '1px solid var(--danger)'
      : '1px solid rgba(0, 0, 0, 0.1)'};
  outline: none;
  cursor: pointer;
  user-select: none;
  transition: all 0.5s ease;

  select {
    display: none;
  }

  &:hover {
    border-bottom: 1px solid var(--cta);
  }
`

const SelectInputLabel = styled.p<ISelectStyles>`
  width: ${({ width }) => (width ? `${width}px` : 'auto')};
`

const SelectInputIcon = styled.div<ISelectChevronStyles>`
  color: var(--grey_900);
  transform: ${({ rotate }) => (rotate === 'true' ? 'rotate(-180deg)' : '')};
  transition: all 0.5s ease;
`

const SelectDropdown = styled.ul<ISelectDropdownStyles>`
  width: 100%;
  height: ${({ height }) => (height ? `${height}px` : 'auto')};
  background-color: white;
  max-height: 150px;
  display: ${({ active }) => (active ? 'flex' : 'none')};
  flex-direction: column;
  position: absolute;
  list-style: none;
  box-shadow: 0 8px 24px -12px rgba(0, 0, 0, 0.4);
  border-radius: 0 0 0.5rem 0.5rem;
  overflow-y: scroll;
  z-index: 5;

  &::-webkit-scrollbar {
    width: 5px;
    border-left: 1px solid var(--grey_200);
  }

  &::-webkit-scrollbar-track {
    background-color: transparent;
  }

  &::-webkit-scrollbar-thumb {
    background-color: var(--grey_700);
    border-radius: 1rem;
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, 0);
  }

  li {
    width: 100%;
    padding: 0.5rem;
    padding-left: 1rem;
    user-select: none;
    cursor: pointer;
    transition: all 0.25s ease;

    &.clear-field {
      color: var(--danger);
    }

    &.disable {
      opacity: 0.5;
      cursor: not-allowed;

      &:hover {
        background-color: transparent;
      }
    }

    &:hover {
      background-color: var(--grey_100);
    }
  }
`

export {
  SelectBody,
  SelectLabel,
  SelectInput,
  SelectView,
  SelectDropdown,
  SelectInputLabel,
  SelectInputIcon,
}
