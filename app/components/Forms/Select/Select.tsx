import {
  SelectBody,
  SelectDropdown,
  SelectInput,
  SelectInputIcon,
  SelectInputLabel,
  SelectView,
} from './Select.styles'
import { ISelect } from './Select.types'
import { useEffect, useRef, useState } from 'react'
import Icon from '@app/components/Icon/Icon'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

function Select({
  selectLabel,
  fullWidth,
  width,
  options,
  height,
  withClear,
  required,
  onSelect,
  requireFail,
}: ISelect): JSX.Element {
  const [menuOpen, setMenuOpen] = useState<boolean>(false)
  const [label, setLabel] = useState('')
  const [value, setValue] = useState('')
  const inputRef = useRef<any>()

  useEffect(() => {
    const handleOutsideClick = (event: any) => {
      if (!inputRef.current) {
        return
      }
      if (!inputRef.current.contains(event.target)) {
        setMenuOpen(false)
      }
    }

    window.addEventListener('mousedown', handleOutsideClick)

    return () => {
      window.removeEventListener('mousedown', handleOutsideClick)
    }
  }, [inputRef])

  const handleOptionSelect = (option: any) => {
    setLabel(option.label)
    setValue(option.value)
    onSelect(option.value)
    setMenuOpen(false)
  }

  const handleClear = () => {
    if (value !== '') {
      setLabel('')
      setValue('')
      setMenuOpen(false)
    }
  }

  return (
    <SelectBody fullWidth={fullWidth} active={menuOpen}>
      <SelectView ref={inputRef}>
        <SelectInput
          active={menuOpen}
          danger={requireFail}
          onClick={() => setMenuOpen(true)}>
          <SelectInputLabel width={width} active={menuOpen}>
            {label !== '' ? label : `${selectLabel} ${required ? '*' : ''}`}
          </SelectInputLabel>
          <SelectInputIcon rotate={menuOpen.toString()}>
            <Icon icon={faChevronDown} />
          </SelectInputIcon>
        </SelectInput>

        <SelectDropdown height={height} active={menuOpen}>
          {withClear && (
            <li
              className={`${
                value === '' ? 'clear-field disable' : 'clear-field'
              }`}
              value={value}
              onClick={handleClear}>
              Auswahl entfernen
            </li>
          )}
          {options.map((option, index) => {
            return (
              <li
                key={index}
                value={option.value}
                onClick={() => handleOptionSelect(option)}>
                {option.label}
              </li>
            )
          })}
        </SelectDropdown>
      </SelectView>
    </SelectBody>
  )
}

export default Select
