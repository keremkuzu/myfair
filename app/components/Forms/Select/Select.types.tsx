export type ISelectDropdownStyles = {
  height?: number
  active: boolean
}

export type ISelectChevronStyles = {
  rotate: string
}

export type ISelectStyles = {
  fullWidth?: boolean
  width?: number
  danger?: boolean
  active: boolean
}

export type ISelect = {
  width?: number
  height?: number
  selectLabel: string
  fullWidth?: boolean
  options: any[]
  withClear?: boolean
  required?: boolean
  onSelect(value: any): void
  requireFail?: boolean
}
