import styled from 'styled-components'
import { ITextFieldStyles } from './TextField.types'

const Label = styled.div<ITextFieldStyles>`
  position: absolute;
  padding: 0 0.5rem;
  font-size: ${props =>
    props.withValue
      ? '0.7rem'
      : props.fontSize === 'xs'
      ? '0.75rem'
      : props.fontSize === 's'
      ? '1rem'
      : props.fontSize === 'm'
      ? '1.25rem'
      : props.fontSize === 'l'
      ? '1.5rem'
      : '1rem'}; // Fallback to S as default
  top: ${props => (props.withValue ? '0' : '50%')};
  left: 0.5rem;
  transform: translateY(-50%);
  background-color: white;
  color: ${props => (props.withValue ? 'var(--cta)' : 'var(--grey_600)')};
  transition: all 200ms ease;
  font-weight: 600;
`

const InputField = styled.input<ITextFieldStyles>`
  width: 100%;
  padding: ${({ padding }) =>
    padding === 'xs'
      ? '4px 8px'
      : padding === 's'
      ? '8px 12px'
      : padding === 'm'
      ? '12px 16px'
      : padding === 'l'
      ? '16px 24px'
      : '8px 12px'}; // Fallback to S as default
  font-size: ${({ fontSize }) =>
    fontSize === 'xs'
      ? '0.75rem'
      : fontSize === 's'
      ? '1rem'
      : fontSize === 'm'
      ? '1.25rem'
      : fontSize === 'l'
      ? '1.5rem'
      : '1rem'}; // Fallback to S as default
  border: 1px solid var(--grey_400);
  border-radius: 0.375rem;
  outline: none;
`

const InputContainer = styled.div`
  width: 100%;
  position: relative;

  &:target ${Label}, &:focus-within ${Label}, &:hover ${Label} {
    top: 0;
    font-size: 0.7rem;
    color: var(--cta);
  }
`

export { InputContainer, InputField, Label }
