import { useState } from 'react'
import { InputContainer, InputField, Label } from './TextField.styles'
import type { ITextField } from './TextField.types'

const TextField = ({
  title,
  type,
  autoCorrect,
  size,
  required,
  value,
  autoComplete,
  name,
  onChange,
}: ITextField): JSX.Element => {
  return (
    <InputContainer>
      <Label
        withValue={value !== ''}
        padding={size || 's'}
        fontSize={size || 's'}>
        {required ? `${title} *` : title}
      </Label>
      <InputField
        withValue={value !== ''}
        fontSize={size || 's'}
        padding={size || 's'}
        name={name}
        autoComplete={autoComplete || 'off'}
        autoCorrect={autoCorrect || 'off'}
        type={type}
        value={value}
        onChange={onChange}
        required={required}
      />
    </InputContainer>
  )
}

export default TextField
