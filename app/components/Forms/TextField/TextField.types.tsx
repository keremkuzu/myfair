export interface ITextFieldStyles {
  padding: 'xs' | 's' | 'm' | 'l'
  fontSize: 'xs' | 's' | 'm' | 'l'
  withValue: boolean
}

export interface ITextField<T = any> {
  type: string
  autoCorrect?: 'on' | 'off'
  size?: 'xs' | 's' | 'm' | 'l'
  title: string
  required?: boolean
  autoComplete?: string
  name?: string
  value: string
  onChange?: (value: T) => void
}
