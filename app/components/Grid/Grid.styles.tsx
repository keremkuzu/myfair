import styled from 'styled-components'
import { IGridStyles } from './Grid.types'

const GridContainer = styled.div<IGridStyles>`
  width: 100%;
  display: grid;
  grid-template-columns: ${props => props.columns};
  grid-gap: ${props => (props.gap ? props.gap : '0')};
  grid-auto-flow: ${props => props.autoflow || 'column'};
`

export { GridContainer }
