import { GridContainer } from './Grid.styles'
import { IGrid } from './Grid.types'

const Grid = ({
  children,
  gridColumns,
  gap,
  autoflow,
  ...props
}: IGrid): JSX.Element => {
  return (
    <GridContainer
      {...props}
      columns={gridColumns}
      gap={gap}
      autoflow={autoflow}>
      {children}
    </GridContainer>
  )
}

export default Grid
