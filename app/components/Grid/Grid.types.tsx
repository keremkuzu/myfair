export type IGridStyles = {
  columns: string
  gap?: string
  autoflow?: string
}

export interface IGrid {
  children: React.ReactNode
  gridColumns: string
  gap?: string
  autoflow?: string
}
