import styled from 'styled-components'

const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export { IconContainer }
