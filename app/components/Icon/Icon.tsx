import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IIcon } from './Icon.types'
import { IconContainer } from './Icon.styles'

function Icon({ icon, size }: IIcon): JSX.Element {
  return (
    <IconContainer>
      <FontAwesomeIcon
        icon={icon}
        fontSize={size}
        style={{ width: size || '1rem', height: size || '1rem' }}
      />
    </IconContainer>
  )
}

export default Icon
