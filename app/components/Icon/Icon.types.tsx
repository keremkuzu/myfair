import { IconDefinition, SizeProp } from '@fortawesome/fontawesome-svg-core'

export type IIcon = {
  icon: IconDefinition
  size?: string | number
}
