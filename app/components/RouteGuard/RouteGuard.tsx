import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { userService } from '@services'
import LoadingScreen from '@app/layouts/LoadingScreen/LoadingScreen'

function RouteGuard({ children }: any) {
  const router = useRouter()
  const [initialized, setInitialized] = useState(false)
  const [authorized, setAuthorized] = useState(false)

  useEffect(() => {
    const checkAuthorization = () => {
      const publicPaths = ['/']
      const path = router.asPath.split('?')[0]

      if (!userService.userValue && !publicPaths.includes(path)) {
        setAuthorized(false)
        router.push('/')
      } else if (userService.userValue && publicPaths.includes(path)) {
        setAuthorized(true)
        router.push('/dashboard')
      } else {
        setAuthorized(true)
      }

      setInitialized(true)
    }

    checkAuthorization()

    const unsubscribe = userService.user.subscribe(() => {
      checkAuthorization()
    })

    return () => {
      unsubscribe.unsubscribe()
    }
  }, [])

  return initialized ? authorized ? children : null : <LoadingScreen />
}

export { RouteGuard }
