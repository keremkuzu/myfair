import Link from 'next/link'
import styled from 'styled-components'

const NavSpan = styled.span`
  width: 100%;
  height: 100%;
  color: var(--grey_900);
`

const NavLinkView = styled(Link)`
  width: 100%;
  padding: 0.5rem 0.75rem;
  display: flex;
  border-radius: 0.375rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 0.5rem;
  text-decoration: none;
  background-color: transparent;
  transition: all 0.25s ease;

  &:hover {
    background-color: var(--grey_200);

    &.--active {
      opacity: 0.75;
    }
  }

  &.--active {
    background-color: var(--cta);

    ${NavSpan}, svg {
      color: var(--white);
    }
  }

  svg {
    color: var(--grey_1000);
  }
`

export { NavLinkView, NavSpan }
