import Link from 'next/link'
import { INavLink } from './NavLink.types'
import { NavLinkView, NavSpan } from './NavLink.styles'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Icon from '@app/components/Icon/Icon'

const NavLink = ({ children, href, icon }: INavLink) => {
  const [active, setActive] = useState<boolean>(false)
  const Router = useRouter()

  useEffect(() => {
    switch (active) {
      case Router.pathname.split('/')[1] === href:
        setActive(false)
        break
      default:
        setActive(true)
        break
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <NavLinkView href={href} className={active ? '--active' : ''}>
      <Icon icon={icon} size="1.125rem" />
      <NavSpan>{children}</NavSpan>
    </NavLinkView>
  )
}

export default NavLink
