import { IconDefinition } from '@fortawesome/fontawesome-svg-core'

export type INavLink = {
  href: string
  children: React.ReactNode
  icon: IconDefinition
}
