import styled from 'styled-components'

const SidebarContainer = styled.div`
  width: 364px;
  height: 100%;
  display: flex;
  flex-direction: row;
  background-color: white;
  box-shadow: 0 0 16px -8px rgba(0, 0, 0, 0.4);
  user-select: none;
`

const SidebarControls = styled.div`
  width: 4rem;
  height: 100%;
  padding: 1rem 0;
  background-color: var(--contrast);
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: center;
`

const SidebarNavigation = styled.nav`
  flex: 1;
  padding: 4rem 2rem;
  display: flex;
  flex-direction: column;
  gap: 2rem;
`

const LogoView = styled.div`
  width: auto;
  height: 48px;
  display: flex;
  justify-content: center;

  img {
    width: inherit;
    height: inherit;
  }
`

const SidebarListView = styled.ul`
  list-style: none;
`

const SidebarListFlex = styled.div`
  margin-bottom: 2rem;
  display: flex;
  flex-direction: column;
  gap: 0.5rem;

  &:last-child {
    margin-bottom: 0;
  }
`

const SidebarListItem = styled.li`
  width: 100%;
  list-style: none;
`

const SidebarListTitle = styled.p`
  width: 100%;
  color: var(--grey_500);
  padding-bottom: 0.5rem;
  font-size: 0.75rem;
  position: relative;
  user-select: none;

  &::after {
    content: '';
    width: 100%;
    height: 1px;
    background-color: var(--grey_400);
    position: absolute;
    left: 0;
    bottom: 0;
  }
`

export {
  LogoView,
  SidebarContainer,
  SidebarControls,
  SidebarNavigation,
  SidebarListView,
  SidebarListItem,
  SidebarListTitle,
  SidebarListFlex,
}
