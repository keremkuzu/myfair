import {
  LogoView,
  SidebarContainer,
  SidebarControls,
  SidebarListFlex,
  SidebarListItem,
  SidebarListTitle,
  SidebarListView,
  SidebarNavigation,
} from './Sidebar.styles'
import Logo from '@assets/logo/logo_text_dark.svg'
import Image from 'next/image'
import NavLink from './NavLink/NavLink'
import Icon from '@app/components/Icon/Icon'
import {
  faCalendar,
  faFileContract,
  faHelmetSafety,
  faMoneyCheckDollar,
  faTableColumns,
} from '@fortawesome/free-solid-svg-icons'

const routes = [
  {
    title: 'Allgemein',
    modules: [
      { module: 'Dashboard', href: 'dashboard', icon: faTableColumns },
      { module: 'Kalender', href: 'calendar', icon: faCalendar },
      {
        module: 'Mitarbeiterverwaltung',
        href: 'maintainers',
        icon: faHelmetSafety,
      },
    ],
  },
  {
    title: 'Aufgaben',
    modules: [
      { module: 'Aufträge', href: 'orders', icon: faMoneyCheckDollar },
      { module: 'Vertragsmanagement', href: 'contracts', icon: faFileContract },
    ],
  },
]

function Sidebar() {
  return (
    <SidebarContainer>
      <SidebarNavigation>
        <LogoView>
          <Image
            src={Logo}
            alt="Logo"
            placeholder="blur"
            blurDataURL="@assets/logo/logo_notext_dark.svg"
            priority
          />
        </LogoView>

        <SidebarListView>
          {routes.map((item, index) => {
            return (
              <SidebarListFlex key={index}>
                <SidebarListTitle>{item.title}</SidebarListTitle>
                {item.modules.map((module, moduleIndex) => {
                  return (
                    <SidebarListItem key={moduleIndex}>
                      <NavLink href={module.href} icon={module.icon}>
                        {module.module}
                      </NavLink>
                    </SidebarListItem>
                  )
                })}
              </SidebarListFlex>
            )
          })}
        </SidebarListView>
      </SidebarNavigation>
    </SidebarContainer>
  )
}

export default Sidebar
