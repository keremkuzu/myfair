import styled from 'styled-components'
import { ITextStyles } from './Text.types'

const TextBody = styled.p<ITextStyles>`
  font-weight: ${props => props.fontWeight || 'normal'};
  font-size: ${props => props.fontSize || '1rem'};
`

export { TextBody }
