import { TextBody } from './Text.styles'
import { IText } from './Text.types'

const Text = ({ children, fontWeight, fontSize }: IText) => {
  return (
    <>
      <TextBody fontWeight={fontWeight} fontSize={fontSize}>
        {children}
      </TextBody>
    </>
  )
}

export default Text
