export type ITextStyles = {
  fontWeight?: string
  fontSize?: string
}

export type IText = {
  children: React.ReactNode
  fontWeight?:
    | '100'
    | '200'
    | '300'
    | '400'
    | '500'
    | '600'
    | '700'
    | '800'
    | '900'
  fontSize?: string
}
