import styled from 'styled-components'

const TitleH1 = styled.h1``

const TitleH2 = styled.h2``

const TitleH3 = styled.h3``

const TitleH4 = styled.h4``

const TitleH5 = styled.h5``

export { TitleH1, TitleH2, TitleH3, TitleH4, TitleH5 }
