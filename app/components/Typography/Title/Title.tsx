import { TitleH1, TitleH2, TitleH3, TitleH4, TitleH5 } from './Title.styles'
import { ITitle } from './Title.types'

const Title = ({ children, type }: ITitle) => {
  return (
    <>
      {type === 'h1' && <TitleH1>{children}</TitleH1>}
      {type === 'h2' && <TitleH2>{children}</TitleH2>}
      {type === 'h3' && <TitleH3>{children}</TitleH3>}
      {type === 'h4' && <TitleH4>{children}</TitleH4>}
      {type === 'h5' && <TitleH5>{children}</TitleH5>}
    </>
  )
}

export default Title
