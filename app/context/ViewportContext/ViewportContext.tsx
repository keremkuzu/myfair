import React, { createContext, useContext, useEffect, useState } from 'react'
import { isServer } from '@services'
import type { ViewPort } from './ViewportContext.types'

export const useViewport = (): ViewPort => {
  const [viewPort, setViewPort] = useState<ViewPort>('ssr')

  const handleResize = (): void => {
    if (isServer) setViewPort('ssr')
    else if (window.matchMedia('(min-width: 1280px)').matches)
      setViewPort('desktop')
    else setViewPort('invalid')
  }

  useEffect(() => {
    handleResize()
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  })

  return viewPort
}
