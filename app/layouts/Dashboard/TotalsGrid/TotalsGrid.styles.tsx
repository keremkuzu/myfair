import styled from 'styled-components'

const TotalsItem = styled.div`
  width: 100%;
  padding: 1rem;
  background-color: var(--white);
  border-radius: 0.75rem;
  box-shadow: var(--box_shadow);
  position: relative;
`

const TotalsTitle = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 1rem;

  svg {
    color: var(--cta);
  }

  span {
    color: var(--grey_1000);
    font-weight: 500;
    font-size: 1.125rem;
  }
`

const TotalsValue = styled.div`
  padding-top: 2rem;
  padding-right: 1rem;
  font-size: 2.5rem;
`

const TotalsBackground = styled.div`
  width: inherit;
  height: auto;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  user-select: none;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    user-select: none;
    pointer-events: none;
    object-fit: contain;
    transform: translateX(12.5%) translateY(25%) scale(1.5);
    opacity: 0.25;
  }
`

export { TotalsItem, TotalsTitle, TotalsValue, TotalsBackground }
