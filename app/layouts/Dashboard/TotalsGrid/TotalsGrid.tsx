import Grid from '@app/components/Grid/Grid'
import { TotalsItem, TotalsTitle, TotalsValue } from './TotalsGrid.styles'
import Title from '@app/components/Typography/Title/Title'
import Icon from '@app/components/Icon/Icon'
import {
  faCalendarDay,
  faFileContract,
  faMoneyBillTransfer,
  faMoneyBillTrendUp,
  faTruck,
} from '@fortawesome/free-solid-svg-icons'

function TotalsGrid() {
  return (
    <Grid gridColumns="repeat(5, minmax(0, 1fr))" gap="2rem" autoflow="column">
      <TotalsItem>
        <TotalsTitle>
          <Icon icon={faFileContract} size="1.5rem" />
          <span>Aufträge in Arbeit</span>
        </TotalsTitle>
        <TotalsValue>
          <Title type="h4">0</Title>
        </TotalsValue>
      </TotalsItem>
      <TotalsItem>
        <TotalsTitle>
          <Icon icon={faCalendarDay} size="1.5rem" />
          <span>Bevorstehende Termine/Fristen</span>
        </TotalsTitle>
        <TotalsValue>
          <Title type="h4">0</Title>
        </TotalsValue>
      </TotalsItem>
      <TotalsItem>
        <TotalsTitle>
          <Icon icon={faTruck} size="1.5rem" />
          <span>Verfügbare Fahrzeuge</span>
        </TotalsTitle>
        <TotalsValue>
          <Title type="h4">0</Title>
        </TotalsValue>
      </TotalsItem>
      <TotalsItem>
        <TotalsTitle>
          <Icon icon={faMoneyBillTrendUp} size="1.5rem" />
          <span>Einnahmen insgesamt</span>
        </TotalsTitle>
        <TotalsValue>
          <Title type="h4">0 €</Title>
        </TotalsValue>
      </TotalsItem>
      <TotalsItem>
        <TotalsTitle>
          <Icon icon={faMoneyBillTransfer} size="1.5rem" />
          <span>Ausgaben insgesamt</span>
        </TotalsTitle>
        <TotalsValue>
          <Title type="h4">0 €</Title>
        </TotalsValue>
      </TotalsItem>
    </Grid>
  )
}

export default TotalsGrid
