import styled, { keyframes } from 'styled-components'

const LoadingBody = styled.div`
  width: 100%;
  height: 100%;
  background-color: #242424;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    width: 256px;
    height: auto;
    margin-bottom: 3rem;
  }
`

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 1rem;
  color: white;
  font-size: 1.25rem;

  svg {
    animation: ${rotate} 3s linear infinite;
  }
`

export { LoadingBody, LoadingContainer }
