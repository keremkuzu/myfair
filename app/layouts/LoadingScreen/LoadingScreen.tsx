import Image from 'next/image'
import { LoadingBody, LoadingContainer } from './LoadingScreen.styles'
import LogoTextLight from '@assets/logo/logo_text_light.svg'
import GlobalStyle from '@app/styles.global'
import styled from 'styled-components'
import Icon from '@app/components/Icon/Icon'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: block;
`

function LoadingScreen() {
  return (
    <>
      <GlobalStyle />
      <Wrapper>
        <LoadingBody>
          <Image
            alt="logo_text_light"
            src={LogoTextLight}
            placeholder="blur"
            blurDataURL="@assets/logo/logo_notext_dark.svg"
            priority
          />

          <LoadingContainer>
            <Icon icon={faSpinner} />
            <p>Ladevorgang</p>
          </LoadingContainer>
        </LoadingBody>
      </Wrapper>
    </>
  )
}

export default LoadingScreen
