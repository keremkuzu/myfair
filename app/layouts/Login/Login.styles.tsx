import styled from 'styled-components'

const Background = styled.div`
  width: 100%;
  height: 100%;
  background-image: linear-gradient(
      45deg,
      rgba(34, 36, 41, 0.75),
      rgba(34, 36, 41, 0.875)
    ),
    url('/img/company.jpg');
  background-size: cover;
  background-position: center center;
  position: relative;
`

const Blur = styled.div`
  width: inherit;
  height: inherit;
  backdrop-filter: blur(8px);
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
`

const LoginContainer = styled.div`
  width: 640px;
  height: 100%;
  margin-right: auto;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  z-index: 2;
`

const Viewport = styled.div`
  width: 100%;
  height: 100%;
  padding: 64px;
  display: flex;
  flex-direction: column;
  overflow-x: hidden;

  img {
    user-select: none;
  }
`

const ViewSwitch = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`

const FormContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const FormText = styled.div`
  margin-bottom: 2rem;

  h3 {
    font-size: 2rem;
    margin-bottom: 1rem;
  }
`

const ButtonRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 1rem;
`

const Footer = styled.footer`
  width: 100%;
  height: 64px;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  justify-content: center;
  align-items: center;
`

const LoginForm = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 1rem;
`

const LoginErrorMessage = styled.div`
  color: var(--danger);
`

const RegisterForm = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 1rem;
`

export {
  Background,
  Blur,
  ButtonRow,
  LoginContainer,
  Viewport,
  FormContainer,
  LoginForm,
  Footer,
  FormText,
  LoginErrorMessage,
  ViewSwitch,
  RegisterForm,
}
