import { useMutation } from '@apollo/client'
import Button from '@app/components/Button/Button'
import Select from '@app/components/Forms/Select/Select'
import TextField from '@app/components/Forms/TextField/TextField'
import LogoTextDark from '@assets/logo/logo_text_dark.svg'
import { LOGIN_MUTATION, REGISTER_USER_MUTATION } from '@lib'
import { AnimatePresence, motion } from 'framer-motion'
import Image from 'next/image'
import { useState } from 'react'
import {
  Background,
  Blur,
  ButtonRow,
  Footer,
  FormContainer,
  FormText,
  LoginContainer,
  LoginErrorMessage,
  LoginForm,
  RegisterForm,
  ViewSwitch,
  Viewport,
} from './Login.styles'

function RegisterView({
  onBackToLoginClick,
  onRegisterSuccess,
}: {
  onBackToLoginClick: () => void
  onRegisterSuccess: () => void
}) {
  const [gender, setGender] = useState('')
  const [genderSet, setGenderError] = useState(false)
  const [registerName, setRegisterName] = useState('')
  const [registerPassword, setRegisterPassword] = useState('')
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [isExisting, setIsExisting] = useState('')

  const [performRegisterUser] = useMutation(REGISTER_USER_MUTATION)

  const genders = [
    { value: 'male', label: 'Herr' },
    { value: 'female', label: 'Frau' },
  ]

  const handleSelectGender = (selectedValue: any) => {
    setGender(selectedValue)
  }

  const usernameInputHandler = (value: string): void => {
    setRegisterName(value)
  }

  const passwordInputHandler = (value: string): void => {
    setRegisterPassword(value)
  }

  const nameHandler = (value: string): void => {
    setFirstname(value)
  }

  const lastnameHandler = (value: string): void => {
    setLastname(value)
  }

  const handleRegistration = async (e: any) => {
    e.preventDefault()

    if (!gender) {
      setGenderError(true)
    }

    try {
      const { data } = await performRegisterUser({
        variables: {
          firstName: firstname,
          lastName: lastname,
          gender: gender,
          email: registerName,
          password: registerPassword,
        },
      })

      setIsExisting('')
      onRegisterSuccess()
    } catch (err: any) {
      console.error(err)
    }
  }

  return (
    <>
      <FormContainer>
        <FormText>
          <h3>Jetzt Registrieren</h3>
          <p>Verwenden Sie für die Registrierung Ihre Firmen-E-Mail Adresse.</p>
        </FormText>

        <RegisterForm onSubmit={(e: any) => handleRegistration(e)}>
          <Select
            width={128}
            selectLabel="Geschlecht"
            options={genders}
            onSelect={handleSelectGender}
            withClear
            required
            requireFail={genderSet}
          />

          <TextField
            title="Name"
            type="text"
            value={firstname}
            name="firstname"
            autoComplete="given-name"
            onChange={e => nameHandler(e.target.value)}
            required
          />
          <TextField
            title="Nachname"
            type="text"
            value={lastname}
            name="lastname"
            autoComplete="family-name"
            onChange={e => lastnameHandler(e.target.value)}
            required
          />
          <TextField
            title="E-Mail"
            type="email"
            value={registerName}
            onChange={e => usernameInputHandler(e.target.value)}
            required
          />
          <TextField
            title="Passwort"
            type="password"
            value={registerPassword}
            onChange={e => passwordInputHandler(e.target.value)}
            required
          />

          {isExisting !== '' && (
            <LoginErrorMessage>
              Ein Benutzer mit dieser E-Mail Adresse existiert bereits.
            </LoginErrorMessage>
          )}

          <ButtonRow>
            <Button btnSize="s" btnType="solid" type="submit">
              Registrieren
            </Button>

            <Button
              btnSize="s"
              btnType="link"
              type="button"
              onClick={onBackToLoginClick}>
              Zurück zum Login
            </Button>
          </ButtonRow>
        </RegisterForm>
      </FormContainer>
    </>
  )
}

function LoginView({
  onRegisterButtonClick,
}: {
  onRegisterButtonClick: () => void
}) {
  const [loginName, setLoginName] = useState('')
  const [loginPassword, setLoginPassword] = useState('')
  const [isError, setIsError] = useState(false)

  const [loginUser] = useMutation(LOGIN_MUTATION)

  const usernameInputHandler = (value: string): void => {
    setLoginName(value)
  }

  const passwordInputHandler = (value: string): void => {
    setLoginPassword(value)
  }

  const handleLogin = async () => {
    try {
      const { data } = await loginUser({
        variables: {
          email: loginName,
          password: loginPassword,
        },
      })
      const { token } = data.loginUser
      const user = {
        firstName: data.loginUser.firstName,
        lastName: data.loginUser.lastName,
        gender: data.loginUser.gender,
        email: data.loginUser.email,
        password: data.loginUser.password,
      }

      // Save token to localStorage client-side
      localStorage.setItem('token', token)
      localStorage.setItem('user', JSON.stringify(user))

      if (isError) setIsError(false)
      window.location.href = '/dashboard'
    } catch (err) {
      console.error(err)
      setIsError(true)
    }
  }

  return (
    <>
      <FormContainer>
        <FormText>
          <h3>Willkommen zurück!</h3>
          <p>
            Loggen Sie sich mit Ihren myfair Management Suite Nutzerdaten ein.
          </p>
        </FormText>

        <LoginForm>
          <TextField
            title="E-Mail"
            type="email"
            autoCorrect="off"
            value={loginName}
            onChange={e => usernameInputHandler(e.target.value)}
            required
          />
          <TextField
            title="Passwort"
            type="password"
            autoCorrect="off"
            value={loginPassword}
            onChange={e => passwordInputHandler(e.target.value)}
            required
          />

          <ButtonRow>
            <Button
              btnSize="xs"
              btnType="ghost"
              type="button"
              npH
              onClick={() => {
                console.log('Not implemented yet')
              }}>
              Passwort vergessen
            </Button>
          </ButtonRow>

          <span />

          <ButtonRow>
            <Button
              type="button"
              btnSize="s"
              btnType="solid"
              onClick={handleLogin}>
              Login
            </Button>
            <Button
              type="button"
              btnSize="s"
              btnType="outline"
              onClick={onRegisterButtonClick}>
              Registrieren
            </Button>
          </ButtonRow>

          {isError && (
            <LoginErrorMessage>
              Nutzername oder Password falsch
            </LoginErrorMessage>
          )}
        </LoginForm>
      </FormContainer>
    </>
  )
}

function LoginLayout() {
  const [isRegistering, setIsRegistering] = useState(false)
  const [registerSuccess, isRegisterSuccess] = useState(false)

  const handleRegisterButtonClick = () => {
    setIsRegistering(true)
    isRegisterSuccess(false)
  }

  const handleBackToLoginButtonClick = () => {
    setIsRegistering(false)
    isRegisterSuccess(false)
  }

  const handleRegisterSuccess = () => {
    setIsRegistering(false)
    isRegisterSuccess(true)
  }

  return (
    <Background>
      <Blur>
        <LoginContainer>
          <Viewport>
            <Image
              src={LogoTextDark}
              alt="logo"
              placeholder="blur"
              blurDataURL="@assets/logo/logo_notext_dark.svg"
              priority
            />

            <ViewSwitch>
              <AnimatePresence mode="wait">
                {isRegistering ? (
                  <motion.div
                    key="register"
                    initial={{ x: '100%', opacity: 0 }}
                    animate={{ x: 0, opacity: 1 }}
                    exit={{ x: '100%', opacity: 0 }}
                    transition={{ duration: 0.5, type: 'tween' }}
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      gap: '2rem',
                    }}>
                    <RegisterView
                      onBackToLoginClick={handleBackToLoginButtonClick}
                      onRegisterSuccess={handleRegisterSuccess}
                    />
                  </motion.div>
                ) : (
                  <motion.div
                    key="login"
                    initial={{ x: '-100%', opacity: 0 }}
                    animate={{ x: 0, opacity: 1 }}
                    exit={{ x: '-100%', opacity: 0 }}
                    transition={{ duration: 0.5, type: 'tween' }}
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      gap: '2rem',
                    }}>
                    <LoginView
                      onRegisterButtonClick={handleRegisterButtonClick}
                    />
                    {registerSuccess && (
                      <p style={{ color: 'var(--success)' }}>
                        Sie haben sich erfolgreich registriert!
                      </p>
                    )}
                  </motion.div>
                )}
              </AnimatePresence>
            </ViewSwitch>
          </Viewport>

          <Footer>&copy; myfair UG &#040;haftungsbeschränkt&#041;</Footer>
        </LoginContainer>
      </Blur>
    </Background>
  )
}

export default LoginLayout
