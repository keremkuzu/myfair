import styled from 'styled-components'

const WarningWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: #f0f0f0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    width: 100%;
    max-width: 256px;
    height: auto;
    margin-bottom: 3rem;
  }
`

const TextContainer = styled.div`
  text-align: center;

  h2 {
    font-size: 1.5em;
    font-weight: bold;
    margin-bottom: 1rem;
  }
`

export { WarningWrapper, TextContainer }
