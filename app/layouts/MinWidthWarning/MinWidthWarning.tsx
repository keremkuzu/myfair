import Image from 'next/image'
import { TextContainer, WarningWrapper } from './MinWidthWarning.styles'
import WarningSVG from '@assets/warning.svg'

function MinWidthWarning() {
  return (
    <WarningWrapper>
      <Image
        src={WarningSVG}
        alt="warning"
        placeholder="blur"
        blurDataURL="@assets/logo/logo_notext_dark.svg"
        priority
      />

      <TextContainer>
        <h2>Hoppla, da ist etwas schiefgelaufen!</h2>
        <p>
          Das myfair Management Suite können Sie leider nicht über mobile
          Endgeräte erreichen.
        </p>
        <p>
          Nutzen Sie daher bitte ein Desktop PC, um das myfair Management Suite
          aufzurufen.
        </p>
      </TextContainer>
    </WarningWrapper>
  )
}

export default MinWidthWarning
