import styled from 'styled-components'

const ModuleContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  background-color: var(--background_1);
`

const Module = styled.div`
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow-y: scroll;

  &::-webkit-scrollbar {
    padding: 0 5px;
    width: 0.925rem;
  }

  &::-webkit-scrollbar-track {
    padding: 0 5px;
    background-color: transparent;
  }

  &::-webkit-scrollbar-thumb {
    background-color: var(--grey_700);
    border-radius: 1rem;
    background-clip: padding-box;
    border: 4px solid rgba(0, 0, 0, 0);
  }
`

const TitleView = styled.div`
  width: 100%;
  padding: 2rem 4rem;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid var(--grey_400);
`

const ModuleBody = styled.div`
  flex: 1;
  padding: 2rem 4rem;
`

const UserControls = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 2rem;
`

export { ModuleContainer, Module, ModuleBody, TitleView, UserControls }
