import Sidebar from '@app/components/Sidebar/Sidebar'
import {
  Module,
  ModuleBody,
  ModuleContainer,
  TitleView,
  UserControls,
} from './ModuleLayout.styles'
import type { IModuleLayout } from './ModuleLayout.types'
import Title from '@app/components/Typography/Title/Title'
import Avatar from '@app/components/Avatar/Avatar'
import { userService } from '@services'
import Text from '@app/components/Typography/Text/Text'
import { useEffect, useState } from 'react'

const ModuleLayout = ({ children, title }: IModuleLayout) => {
  const [greeting, setGreeting] = useState('')
  const firstName = userService.userValue.firstName

  useEffect(() => {
    const currentTime = new Date()
    const currentHour = currentTime.getHours()

    if (currentHour >= 5 && currentHour < 12) {
      setGreeting('Guten Morgen')
    } else if (currentHour >= 12 && currentHour < 10) {
      setGreeting('Guten Tag')
    } else {
      setGreeting('Guten Abend')
    }
  }, [])

  return (
    <ModuleContainer>
      <Sidebar />
      <Module>
        <TitleView>
          <Title type="h1">{title}</Title>
          <UserControls>
            <Text fontSize="1.125rem">
              {greeting}, {firstName}!
            </Text>
            <Avatar />
          </UserControls>
        </TitleView>
        <ModuleBody>{children}</ModuleBody>
      </Module>
    </ModuleContainer>
  )
}

export default ModuleLayout
