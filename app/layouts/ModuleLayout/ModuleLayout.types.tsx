export type IModuleLayout = {
  children?: React.ReactNode
  title: string
}
