import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  //
  // STRUCTURE
  //
  html,
  body {
    height: 100%;
    display: block;
    margin: 0;
    padding: 0;
  }

  body {
    min-width: 0;
    line-height: 1.4;
    overflow-x: hidden;
    overflow-y: auto;
    font-family: 'Inter', sans-serif;
  }

  #__next {
    width: 100%;
    height: 100%;
  }

  *,
  *:before,
  *:after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  //
  // COLORS
  //
  :root {
    --cta: #7098bf;
    --cta: #7098bf;
    --cta2: #7087bf;

    --background_1: #ECECF6;

    --success: #2ecc71;
    --warning: #e67e22;
    --danger: #e74c3c;
    --contrast: #242424;

    --white: #fff;

    --grey_100: #f1f1f1;
    --grey_200: #f0f0f0;
    --grey_300: #f4f4f4;
    --grey_400: #d7d7d7;
    --grey_500: #b9b9b9;
    --grey_600: #9c9c9c;
    --grey_700: #7e7e7e;
    --grey_800: #616161;
    --grey_900: #424242;
    --grey_1000: #242424;

    --box_shadow: 0 8px 16px -12px rgba(0, 0, 0, .4);
  }
`

export default GlobalStyle
