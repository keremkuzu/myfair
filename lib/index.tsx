import { LOGIN_MUTATION } from './mutations/loginUserMutation'
import { REGISTER_USER_MUTATION } from './mutations/registerUserMutation'
import { GET_USERS } from './queries/usersQuery'

export { GET_USERS, LOGIN_MUTATION, REGISTER_USER_MUTATION }
