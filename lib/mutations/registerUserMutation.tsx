import { gql } from '@apollo/client'

export const REGISTER_USER_MUTATION = gql`
  mutation RegisterUser(
    $firstName: String!
    $lastName: String!
    $gender: String!
    $email: String!
    $password: String!
  ) {
    registerUser(
      firstName: $firstName
      lastName: $lastName
      gender: $gender
      email: $email
      password: $password
    ) {
      id
      firstName
      lastName
      gender
      email
    }
  }
`
