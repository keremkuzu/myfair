const Error404 = (): JSX.Element => {

  return <p>404 - Page not found</p>
}

export default Error404
