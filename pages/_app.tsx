import Head from 'next/head'
import '../app/styles.global'

import { RouteGuard } from '@app/components/RouteGuard/RouteGuard'
import type { AppProps } from 'next/app'
import styled from 'styled-components'
import MinWidthWarning from '@app/layouts/MinWidthWarning/MinWidthWarning'
import { useViewport } from '@app/context/ViewportContext/ViewportContext'
import LoadingScreen from '@app/layouts/LoadingScreen/LoadingScreen'
import GlobalStyle from '../app/styles.global'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client'

const client = new ApolloClient({
  uri: 'http://localhost:3001/graphql',
  cache: new InMemoryCache(),
})

export default App

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: block;
`

function App({ Component, pageProps }: AppProps) {
  const viewport = useViewport()

  if (viewport === 'ssr')
    return (
      <>
        <GlobalStyle />
        <Wrapper>
          <LoadingScreen />
        </Wrapper>
      </>
    )

  return (
    <>
      <Head>
        <title>myfair | Management Suite</title>
      </Head>

      <ApolloProvider client={client}>
        <GlobalStyle />
        <Wrapper>
          <RouteGuard>
            {viewport === 'desktop' ? (
              <Component {...pageProps} />
            ) : (
              <MinWidthWarning />
            )}
          </RouteGuard>
        </Wrapper>
      </ApolloProvider>
    </>
  )
}
