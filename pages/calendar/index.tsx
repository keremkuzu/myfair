import Title from '@app/components/Typography/Title/Title'
import ModuleLayout from '@app/layouts/ModuleLayout/ModuleLayout'

function Calendar() {
  return <ModuleLayout title="Kalender"></ModuleLayout>
}

export default Calendar
