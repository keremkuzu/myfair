import Title from '@app/components/Typography/Title/Title'
import ModuleLayout from '@app/layouts/ModuleLayout/ModuleLayout'

function Contracts() {
  return <ModuleLayout title="Vertragsmanagement"></ModuleLayout>
}

export default Contracts
