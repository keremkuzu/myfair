import TotalsGrid from '@app/layouts/Dashboard/TotalsGrid/TotalsGrid'
import ModuleLayout from '@app/layouts/ModuleLayout/ModuleLayout'

function Dashboard() {
  return (
    <ModuleLayout title="Dashboard">
      <TotalsGrid />
    </ModuleLayout>
  )
}

export default Dashboard
