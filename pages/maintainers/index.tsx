import Title from '@app/components/Typography/Title/Title'
import ModuleLayout from '@app/layouts/ModuleLayout/ModuleLayout'

function Maintainers() {
  return <ModuleLayout title="Mitarbeiterverwaltung"></ModuleLayout>
}

export default Maintainers
