import Title from '@app/components/Typography/Title/Title'
import ModuleLayout from '@app/layouts/ModuleLayout/ModuleLayout'

function Orders() {
  return <ModuleLayout title="Aufträge"></ModuleLayout>
}

export default Orders
