const isBrowser: boolean = typeof window !== 'undefined'

const isServer = !isBrowser

export { isBrowser, isServer }
