export const sizes = {
  mobile: 360,
  smallTablet: 768,
  largeTablet: 1024,
  desktop: 1440,
}

export const queries = {
  mobile: `(min-width: ${sizes.mobile}px)`,
  smallTablet: `(min-width: ${sizes.smallTablet}px)`,
  largeTablet: `(min-width: ${sizes.largeTablet}px)`,
  desktop: `(min-width: ${sizes.desktop}px)`,
}

export const queriesReversed = {
  mobile: `(max-width: ${sizes.mobile - 1}px)`,
  smallTablet: `(max-width: ${sizes.smallTablet - 1}px)`,
  largeTablet: `(max-width: ${sizes.largeTablet - 1}px)`,
  desktop: `(max-width: ${sizes.desktop - 1}px)`,
}

export const mq = {
  mobile: `@media only screen and (min-width: ${sizes.mobile}px)`,
  smallTablet: `@media only screen and (min-width: ${sizes.smallTablet}px)`,
  largeTablet: `@media only screen and (min-width: ${sizes.largeTablet}px)`,
  desktop: `@media only screen and (min-width: ${sizes.desktop}px)`,
}

export const mqReversed = {
  mobile: `@media only screen and (max-width: ${sizes.mobile - 1}px)`,
  smallTablet: `@media only screen and (max-width: ${sizes.smallTablet - 1}px)`,
  largeTablet: `@media only screen and (max-width: ${sizes.largeTablet - 1}px)`,
  desktop: `@media only screen and (max-width: ${sizes.desktop - 1}px)`,
}
