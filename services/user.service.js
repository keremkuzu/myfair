import { BehaviorSubject } from 'rxjs'

const userSubject = new BehaviorSubject(
  typeof window !== 'undefined' && JSON.parse(localStorage.getItem('user')),
)

export const userService = {
  user: userSubject.asObservable(),
  get userValue() {
    return userSubject.value
  },
  logout,
  updateUserValue,
}

function logout() {
  // remove user from local storage, publish null to user subscribers and redirect to login page
  localStorage.removeItem('user')
  localStorage.removeItem('token')
  userSubject.next(null)
  window.location.href = '/'
}

function updateUserValue(user) {
  localStorage.setItem('user', JSON.stringify(user))
  userSubject.next(user)
}
